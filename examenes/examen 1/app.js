let ListaCompleta;

async function JsonPendientes() {
    try {
        const response = await fetch('http://jsonplaceholder.typicode.com/todos');
        if (!response.ok) {
            throw new Error('No se pudo obtener la lista de pendientes');
        }
        const data = await response.json();
        console.log('¡Se ha conectado correctamente!');
        ListaCompleta = data;
    } catch (error) {
        console.error('Error:', error);
    }
}

// Función para mostrar Lista de pendientes (solo IDs)
function ShowSoloIDs() {
    const lista = ListaCompleta;
    const listaContainer = document.getElementById('lista-container');
    listaContainer.innerHTML = '';
    const listaIds = lista.map(item => item.id).join(', ');
    listaContainer.innerHTML = 'Lista: ' + listaIds;
}

// Función para mostrar Lista de todos los pendientes (IDs y titles)
function ShowIDsTitles() {
    const lista = ListaCompleta;
    const listaContainer = document.getElementById('lista-container');
    listaContainer.innerHTML = '';
    const listaIds = lista.map(item => `ID: ${item.id}, Título: ${item.title}`).join('<br>');
    listaContainer.innerHTML = 'Lista: <br>' + listaIds;
}

// Función para mostrar Lista de todos los pendientes sin resolver (IDs y titles)
function ShowSinResolverIDsTitles() {
    const listaFiltrada = ListaCompleta.filter(item => !item.completed);
    const listaContainer = document.getElementById('lista-container');
    listaContainer.innerHTML = '';
    const lista = listaFiltrada.map(item => `ID: ${item.id}, Título: ${item.title}`).join('<br>');
    listaContainer.innerHTML = 'Lista: <br>' + lista;
}

// Función para mostrar Lista de todos los pendientes resueltos (IDs y titles)
function ShowResueltosIDsTitles() {
    const listaFiltrada = ListaCompleta.filter(item => item.completed);
    const listaContainer = document.getElementById('lista-container');
    listaContainer.innerHTML = '';
    const lista = listaFiltrada.map(item => `ID: ${item.id}, Título: ${item.title}`).join('<br>');
    listaContainer.innerHTML = 'Lista: <br>' + lista;
}

// Función para mostrar Lista de todos los pendientes (IDs y userID)
function ShowIDsUserID() {
    const lista = ListaCompleta;
    const listaContainer = document.getElementById('lista-container');
    listaContainer.innerHTML = '';
    const listaIds = lista.map(item => `ID: ${item.id}, UserID: ${item.userId}`).join('<br>');
    listaContainer.innerHTML = 'Lista: <br>' + listaIds;
}

// Función para mostrar Lista de todos los pendientes resueltos (IDs y userID)
function ShowResueltosIDsUserID() {
    const listaFiltrada = ListaCompleta.filter(item => item.completed);
    const listaContainer = document.getElementById('lista-container');
    listaContainer.innerHTML = '';
    const lista = listaFiltrada.map(item => `ID: ${item.id}, UserID: ${item.userId}`).join('<br>');
    listaContainer.innerHTML = 'Lista: <br>' + lista;
}

// Función para mostrar Lista de todos los pendientes sin resolver (IDs y userID)
function ShowSinResolverIDsUserID() {
    const listaFiltrada = ListaCompleta.filter(item => !item.completed);
    const listaContainer = document.getElementById('lista-container');
    listaContainer.innerHTML = '';
    const lista = listaFiltrada.map(item => `ID: ${item.id}, UserID: ${item.userId}`).join('<br>');
    listaContainer.innerHTML = 'Lista: <br>' + lista;
}

document.addEventListener('DOMContentLoaded', async function() {
    await JsonPendientes();
    // console.log(ListaCompleta);

    ShowSoloIDs();
});